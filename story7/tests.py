from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index

# Create your tests here.
class TestStory7(TestCase):
    def test_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_using_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)
    
    def test_using_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'story7.html')

    def test_title_ada(self):
        response = Client().get('')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Story 7", html_kembalian)
        self.assertTemplateUsed(response, 'story7.html')

    def test_header_ada(self):
        response = Client().get('')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Track Record", html_kembalian)
        self.assertTemplateUsed(response, 'story7.html')

    def test_judul_accordion_ada(self):
        response = Client().get('')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Riwayat pendidikan", html_kembalian)
        self.assertIn("Aktivitas saat ini", html_kembalian)
        self.assertIn("Pengalaman organisasi", html_kembalian)
        self.assertIn("Prestasi", html_kembalian)
        self.assertTemplateUsed(response, 'story7.html')

    def test_content_riwayat_ada(self):
        response = Client().get('')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("1. SDIT Citra Az-Zahra (2007-2013)", html_kembalian)
        self.assertIn("2. SMPI Al-Azhar 4 (2013-2016)", html_kembalian)
        self.assertIn("3. SMAN 78 Jakarta (2016-2019)", html_kembalian)
        self.assertIn("4. Universitas Indonesia (2019-...)", html_kembalian)
        self.assertTemplateUsed(response, 'story7.html')

    def test_content_aktivitas_ada(self):
        response = Client().get('')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("1. Mahasiswa Fakultas Ilmu Komputer Universitas Indonesia Jurusan Sistem Informasi", html_kembalian)
        self.assertIn("2. Staf divisi bibliografi OSUI Mahawaditra 2020", html_kembalian)
        self.assertTemplateUsed(response, 'story7.html')

    def test_content_organisasi_ada(self):
        response = Client().get('')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("1. Staf divisi media Rohis SMAN 78 Jakarta (2017-2018)", html_kembalian)
        self.assertIn("2. Pemain violin SSD SMAN 78 Jakarta (2016-2019)", html_kembalian)
        self.assertIn("3. Pemain violin OSUI Mahawaditra (2020)", html_kembalian)
        self.assertIn("4. Staf divisi bibliografi OSUI Mahawaditra (2020)", html_kembalian)
        self.assertTemplateUsed(response, 'story7.html')

    def test_content_prestasi_ada(self):
        response = Client().get('')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("1. Juara 1 lomba musikalisasi puisi tingkat sekolah (2017)", html_kembalian)
        self.assertTemplateUsed(response, 'story7.html')


