$('.toggle').click(function(e) {
    e.preventDefault();

    let $var = $(this);

    if ($var.next().hasClass('show')) {
        $var.next().removeClass('show');
        $var.next().slideUp(350);
    } else {
        $var.parent().parent().find('li .inner').removeClass('show');
        $var.parent().parent().find('li .inner').slideUp(350);
        $var.next().toggleClass('show');
        $var.next().slideToggle(350);
    }
});

$(document).ready(function(){
    $(".up,.down").click(function(){
        var row = $(this).parents("li:first");
        if ($(this).is(".up")) {
            row.insertBefore(row.prev());
        } else if ($(this).is(".down")) {
            row.insertAfter(row.next());
        } 
    });
});