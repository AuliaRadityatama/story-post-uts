from django.urls import path
from .views import func_json

app_name = 'story8'

urlpatterns = [
    path('', func_json, name='caribuku'),
]