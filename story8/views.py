from django.shortcuts import render
from django.http import JsonResponse
import requests
import json

# Create your views here.
def func_json(request):
    response = {}
    return render(request,'story8.html',response)

def func_api(request):
    q = request.GET['q']   
    url_tujuan = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + q)
    r = request.get(url_tujuan)

    data = json.loads(r.content)
    return JsonResponse(data, safe=False)
 