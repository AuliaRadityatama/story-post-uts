from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import func_json
from .views import func_api
import requests
import responses
import json

# Create your tests here.
class TestStory8(TestCase):
    def test_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_using_func_json(self):
        found = resolve('/caribuku/')
        self.assertEqual(found.func, func_json)
    
    def test_using_home_html(self):
        response = Client().get('/caribuku/')
        self.assertTemplateUsed(response, 'story8.html')

    def test_title_ada(self):
        response = Client().get('/caribuku/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Story 8", html_kembalian)
        self.assertTemplateUsed(response, 'story8.html')

    def test_header_ada(self):
        response = Client().get('/caribuku/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Cari Buku", html_kembalian)
        self.assertTemplateUsed(response, 'story8.html')

    def test_header_tabel_ada(self):
        response = Client().get('/caribuku/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("No", html_kembalian)
        self.assertIn("Cover", html_kembalian)
        self.assertIn("Judul", html_kembalian)
        self.assertIn("Penulis", html_kembalian)
        self.assertIn("Deskripsi", html_kembalian)
        self.assertIn("Penerbit", html_kembalian)
        self.assertTemplateUsed(response, 'story8.html')
        
    @responses.activate
    def test_api(self):
            responses.add(**{
            'method'         : responses.GET,
            'url'            : 'https://www.googleapis.com/books/v1/volumes?q=frozen',
            'body'           : '{"error": "reason"}',
            'status'         : 404,
            'content_type'   : 'application/json',
            'adding_headers' : {'X-Foo': 'Bar'}
            })
            response = requests.get('https://www.googleapis.com/books/v1/volumes?q=frozen')
            self.assertEqual({'error': 'reason'}, response.json())
            self.assertEqual(404, response.status_code)


