$(document).ready(function() {
    $("#input").on("keyup", function(e) {
        var q = e.currentTarget.value.toLowerCase()
        console.log(q)
        $.ajax({
            url: "https://www.googleapis.com/books/v1/volumes?q=" + q,
            success: function(hasil) {
                console.log(hasil.items)
                $('#list').html('')
                var res = '<tr>';
                for (var i = 0; i < hasil.items.length; i++) {
                    res += "<tr> <th scope='row' class='align-middle text-center'>" + (i + 1) + "</th>" +
                        "<td><img src='" + hasil.items[i].volumeInfo.imageLinks.smallThumbnail + "'></img>" + "</td>" +
                        "<td class='align-middle'>" + hasil.items[i].volumeInfo.title + "</td>" +
                        "<td class='align-middle'>" + hasil.items[i].volumeInfo.authors + "</td>" +
                        "<td class='align-middle'>" + hasil.items[i].volumeInfo.description + "</td>" +
                        "<td class='align-middle'>" + hasil.items[i].volumeInfo.publisher + "</td>"
                }
                $('#list').append(res);
            },
        })
    });
});
